from __future__ import print_function
import os
from time import time
from os import listdir
from os.path import isdir

import sklearn
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.datasets import fetch_20newsgroups
import pandas as pd
import numpy as np
pd.set_option('display.mpl_style', 'default')
pd.set_option('display.width', 5000)
pd.set_option('display.max_columns', 60)
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
matplotlib.style.use('ggplot')
SPINE_COLOR = 'gray'

%matplotlib inline

base_dir = '/home/doaa/Documents/Spring2016_SE_Project/'

# Constants
NUM_TOPICS = 50
max_df = 0.7
min_df = 0.3
# suffix = str(max_df)+'_'+str(min_df) # for 10
lang = 'categories_fixed_'
# lang = 'all_'
# lang = ''
suffix = lang + str(NUM_TOPICS)+'_'+str(max_df)+'_'+str(min_df)
print(suffix)

proj_topic = pd.read_csv(os.path.join(base_dir, 'results/project-topic_'+suffix+'.csv'), index_col=0)
#  normalize values
for i in proj_topic.index:
    proj_topic.ix[i, proj_topic.columns[0:NUM_TOPICS]] = \
                proj_topic.ix[i, proj_topic.columns[0:NUM_TOPICS]]\
                    / (proj_topic.ix[i, proj_topic.columns[0:NUM_TOPICS]].max())
# To date
proj_topic.date = pd.to_datetime(proj_topic.date)
proj_topic.sort_values('date', inplace=True)
print(proj_topic.shape)
proj_topic.head()

topics = pd.read_csv(os.path.join(base_dir, 'results/topic_word_'+suffix+'.csv'), index_col=0)
topics_freq = pd.read_csv(os.path.join(base_dir, 'results/topic_word_freq_'+suffix+'.csv'), index_col=0)

topics.head(10)


# --------------- start here
# -----------------------------------------

# Constants
NUM_TOPICS = 50
max_df = 0.7
min_df = 0.3
lang = 'categories_fixed_'
suffix = lang + str(NUM_TOPICS)+'_'+str(max_df)+'_'+str(min_df)
print(suffix)


projects_details = pd.read_csv(os.path.join(base_dir, 'results/projects_details.csv'), index_col=0)
# projects_details.rename(columns={'index': 'project'}, inplace=True)

topic_word_raw = pd.read_csv(os.path.join(base_dir, 'results/topic_word_raw_'+suffix+'.csv'), index_col=0)
topic_word_raw.shape

import seaborn as sns
from scipy.spatial.distance import cosine


def plot_heatmaps(proj_topic):
    #  normalize values
    for i in proj_topic.index:
        proj_topic.ix[i, proj_topic.columns[0:NUM_TOPICS]] = \
                    proj_topic.ix[i, proj_topic.columns[0:NUM_TOPICS]]\
                        / (proj_topic.ix[i, proj_topic.columns[0:NUM_TOPICS]].max())


    clusters = pd.merge(projects_details, proj_topic, right_index=True, left_index=True)
    clusters.reset_index(inplace=True)
    clusters.sort_values('group', inplace=True)
    df = clusters#[~projects['index'].isin(to_remove)]
    df.set_index('index', inplace=True)
    categories = df.groupby('group')

    f, ax= plt.subplots(2, 3, figsize=(14, 7))#, sharex=True)

    for i, (name, g) in enumerate(categories):
        x = i%2
        y = int(i/2)
        print(name)
        sns.heatmap(g.ix[:,2:NUM_TOPICS+2], ax=ax[x][y])#, cmap="RdBu_r", cbar=False)
        ax[x][y].set_title(name, fontsize=16)
        ax[x][y].set_ylabel('')
        labels = ax[x][y].get_xticklabels()
        plt.setp(labels, rotation=60)

    plt.tight_layout()
    return clusters


proj_topic = pd.read_csv(os.path.join(base_dir, 'results/project-topic_'+suffix+'.csv'), index_col=0)
clusters = plot_heatmaps(proj_topic)

plt.savefig('topic_projects_heatmap'+suffix+'.png', bbox_inches='tight', dpi=350)


# -----------------------------------------------------------------

def method2():
    threshold = 0.6

    mat = topic_word_raw.T

    n = mat.shape[1]
    sim = []
    for i in range(n):
        for j in range(i + 1, n):
            s = 1 - cosine(mat.ix[:, i], mat.ix[:, j])
            if s > threshold:
                print('topic{}-topic{}: {}'.format(i, j, s))
                sim.append([mat.columns[i], mat.columns[j]])

    print(sim)
    mat = clusters[clusters.columns[2:NUM_TOPICS + 2]]
    # merge similar columns (topics)
    for row in sim:
        mat.ix[:, row[0]] = mat.ix[:, row[0]] + mat.ix[:, row[1]]
        # drop columns after adding similarities (don't merge with above, can be recursive)
        mat.ix[:, row[1]] = 0  # empty topic
        mat.replace(row[1], row[0])

# -----------------------------------------------

def method3():
    threshold = 0.5

    def get_cluster_matrix(mat, sim):
        categ = pd.DataFrame()
        # merge similar columns (topics)
        for row in sim:
            cat_name = str(row[0]) + '_' + str(row[1])
            categ[cat_name] = mat.ix[:, row[0]] + mat.ix[:, row[1]]

        # for unique topics copy as separete category
        for i in range(NUM_TOPICS):
            if unique[i] == 1:
                categ[i] = mat.ix[:, i]
        return categ

    mat = topic_word_raw.T
    n = mat.shape[1]
    sim = []
    unique = [1] * NUM_TOPICS
    for i in range(n):
        for j in range(i + 1, n):
            s = 1 - cosine(mat.ix[:, i], mat.ix[:, j])
            if s > threshold:
                print('topic{}-topic{}: {}'.format(i, j, s))
                sim.append([mat.columns[i], mat.columns[j]])
                # mark topic i and topic j as non unique categories
                unique[i], unique[j] = 0, 0

    print(sim)
    mat = clusters[clusters.columns[2:NUM_TOPICS + 2]]
    categ = get_cluster_matrix(mat, sim)

    mat = categ
    print(mat.shape)
    # mat

# -----------------------------------------------

def method5():
    n_clusters = 8

    from sklearn.cluster import AgglomerativeClustering

    X = topic_word_raw
    X_norm = sklearn.preprocessing.Normalizer().fit_transform(X)

    # labels = AgglomerativeClustering(n_clusters=n_clusters,
    #                                      affinity='cosine',
    #                                      linkage='complete').fit_predict(X_norm)

    labels = sklearn.cluster.KMeans(n_clusters=n_clusters).fit_predict(X_norm)

    print("Labels: ", labels)

    # --- topic_word
    cat_word = pd.DataFrame(columns=topic_word_raw.columns)
    # merge similar clusters (topics)
    for i, label in enumerate(labels):
        if label in cat_word.index:
            cat_word.loc[label] = cat_word.loc[label] + X.iloc[i]
        else:
            cat_word.loc[label] = X.iloc[i]

    # ---- Project_cat
    mat = clusters[clusters.columns[2:NUM_TOPICS + 2]]

    proj_cat = pd.DataFrame()
    for i, label in enumerate(labels):
        if label in proj_cat.columns:
            #         print('summing {} + {}'.format(proj_cat[label], mat.ix[:,i]))
            proj_cat[label] = proj_cat[label] + mat.ix[:, i]
        else:
            #         print('assiging: {}'.format(mat.ix[:,i]))
            proj_cat[label] = mat.ix[:, i]

    # cat_word
    # proj_cat
    mat = proj_cat
    mat.head()

# --------------------------------------------------

method5()


# ------------------------------------------------------

c = plot_heatmaps(mat)

# --------> Remove membership values below a threshould
threshold = 0.2
labels = clusters[clusters.columns[0]]
mat = mat.clip_lower(threshold).replace(threshold, 0) # remove values below the threshold
# sns.heatmap(mat)
# print(labels)
c = plot_heatmaps(mat)

# ----------> Binarize the matrix
max_clusters = mat.idxmax(axis=1)
bin_mat = mat.copy()
for i in bin_mat.index:
    bin_mat.ix[i,max_clusters[i]] = 100
bin_mat = bin_mat.clip_lower(99).replace(99, 0).replace(100,1) # remove values below the threshold
# sns.heatmap(bin_mat)
c = plot_heatmaps(bin_mat)

# num of projects per predicted cluster
num_proj_per_cluster = bin_mat.sum()
num_proj_per_cluster
# Number of projects per true clusters
labels.value_counts()

# ----------> Generate clustering accuracy

def precision_soft(index, pred, true):
    p = 0
    intersect = np.arange(0)
    n = 0
#     print(index, '--->', pred.loc[index][pred.loc[index]>0].index.values)
    for topic in pred.loc[index][pred.loc[index]>0].index:
        c_pred_i = topic#pred.loc[index].idxmax() # the column(topics) of max membership
        c_pred = pred[pred[c_pred_i]>0].index.values # all the cluster of 'index'
        c_true = true[true==true[index]].index.values # true clutser members
        intersect2 = np.intersect1d(c_pred, c_true)
        if intersect2.shape[0]>intersect.shape[0]:  # choose larger cluster
            intersect = intersect2
            n = c_pred.shape[0]

    p = intersect.shape[0] / n if n>0 else 0
    return p


def recall_soft(index, pred, true):
    r = 0
    intersect = np.arange(0)
    for topic in pred.loc[index][pred.loc[index]>0].index:
        c_pred_i = topic#pred.loc[index].idxmax() # the column(topics) of max membership
        c_pred = pred[pred[c_pred_i]>0].index.values # all the cluster of 'index'
        c_true = true[true==true[index]].index.values # true clutser members
        intersect2 = np.intersect1d(c_pred, c_true)
        if intersect2.shape[0]>intersect.shape[0]:  # choose larger cluster
            intersect = intersect2

    return intersect.shape[0] / c_true.shape[0]

def get_F_score(precision, recall):
    return 2 * precision * recall / (precision+recall)

# ------------------------------------------------------------------

precision = 0
for i in bin_mat.index:
    p = precision_soft(i, mat, labels)
    precision += p

precision = precision/labels.shape[0]
print('precision', precision)

recall = 0
for i in bin_mat.index:
    p = recall_soft(i, mat, labels)
    recall += p

recall = recall/labels.shape[0]
print('recall', recall)

f_score = get_F_score(precision, recall)
print('f score: ', f_score)

